<?php

/**
 * Implements hook_views_data_alter().
 */
function dupree_case_studies_filter_views_data_alter(array &$data) {
  $data['node_field_data']['related_content_titles'] = array(
    'title' => t('Related content titles'),
    'filter' => array(
      'title' => t('Related content titles'),
      'help' => t('Specify a list of titles a content node can have.'),
      'field' => 'nid',
      'id' => 'mymodule_related_content_titles'
    ),
  );
}