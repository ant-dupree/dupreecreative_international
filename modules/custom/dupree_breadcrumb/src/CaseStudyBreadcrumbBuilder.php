<?php

namespace Drupal\dupree_breadcrumb;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class CaseStudiesBreadcrumb.
 *
 * @package Drupal\dupree_breadcrumb
 */
class CaseStudyBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs the NodeBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entityManager) {
    $this->entityManager = $entityManager;
    $this->nodeStorage = $entityManager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {

    return $route_match->getRouteName() == 'entity.node.canonical'
    && $route_match->getParameter('node')
    && $route_match->getParameter('node')->bundle() == 'portfolio';
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    // Add Our Clients link
    $url_object = \Drupal::service('path.validator')->getUrlIfValid('our-clients');
    $route_name = $url_object->getRouteName();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Our Clients'), $route_name));
    // Add Case Studies Link
    $url_object = \Drupal::service('path.validator')->getUrlIfValid('case-studies');
    $route_name = $url_object->getRouteName();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Case Studies'), $route_name));

    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }
}
