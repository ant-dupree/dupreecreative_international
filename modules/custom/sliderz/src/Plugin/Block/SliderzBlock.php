<?php

namespace Drupal\sliderz\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityManager;

/**
 * Provides a 'SliderzBlock' block.
 *
 * @Block(
 *  id = "sliderz_block",
 *  admin_label = @Translation("Sliderz block"),
 * )
 */
class SliderzBlock extends BlockBase implements ContainerFactoryPluginInterface {


  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entity_query;

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  protected $entity_manager;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct( array $configuration, $plugin_id, $plugin_definition, QueryFactory $entity_query, EntityManager $entityManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entity_query = $entity_query;
    $this->entityManager = $entityManager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.query'),
      $container->get('entity.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function build() {

    $sliderz = array();

    // Get sliderz nodes
    $nids = $this->getSliderzNodes();

    if (count($nids) < 1) {
      return $build['sliderz_block']['#markup'] = $this->t('No nodes with a body field.');
    }

    /**
     * @var $entity_storage \Drupal\Core\Entity\ContentEntityStorageBase
     */
    $entity_storage = $this->entityManager->getStorage('node');

    foreach ($nids as $key => $nid) {
      /**
       * @var $entity \Drupal\Core\Entity\Entity
       */
      $node = $entity_storage->load($nid);
      $sliderz[$key]['message'] = $node->field_sliderz->value;
      $sliderz[$key]['image_url'] = file_create_url($node->field_sliderz_image->entity->getFileUri());
    }

    $build = [];
    $build['#title'] = 'Test Title Scooby Doo';
    $build['#theme'] = 'sliderz_block';
    $build['#sliderz_block']['#markup'] = 'TestAnt';
    $build['#sliderz_slides'] = $sliderz;
    $build['#attached'] = [
        'library' =>  [
            'sliderz/sliderz-block'
        ],
    ];

    return $build;
  }

  /**
   * @return array|int
   */
  public function getSliderzNodes() {
    // @todo : node_storage ????
    //$node_storage = $this->entityManager->getStorage('node');
    // Use the factory to create a query object for node entities.
    $query = $this->entity_query->get('node');
    $query->condition('type', 'sliderz');
    $query->condition('status', 1);
    $query->sort('field_order_by', 'ASC');
    $nids = $query->execute();

    return $nids;
  }

  /**
   * @param $nodeIds
   */
  public function getSliderzAssets($nids) {

    foreach ($nids as  $key => $node) {
      print_r($key);
    }

  }


  /**
   * @return array
   */
  protected function simpleQuery() {
    $query = $this->entity_query->get('node')
      ->condition('status', 1);
    $nids = $query->execute();
    return array_values($nids);
  }


  /**
   * @return array
   */
  public function basicQuery() {
    return [
      '#title' => 'Published Nodes',
      'content' => [
        '#theme' => 'item_list',
        '#items' => $this->simpleQuery()
      ]
    ];
  }
}
