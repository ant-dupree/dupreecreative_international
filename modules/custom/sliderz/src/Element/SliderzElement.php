<?php

namespace Drupal\Sliderz\Element;

use Drupal\Core\Render\Element\RenderElement;



/**
 * Provides an render element for slides.
 *
 * @RenderElement("sliderz_element")
 */
class SliderzElement extends RenderElement {
  
  /********** IGNORE FOR NOW *****************/

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'sliderz_element',
      '#label' => 'Default Label',
      '#description' => 'Default Description',
      '#pre_render' => [
        [$class, 'preRenderSliderzElement'],
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderSliderzElement($element) {

    $element['#image_link'] = 'http://http://eight.dev/themes/custom/konnekt/images/heroes.png';
    $element['#message'] = 'Hello World';

    return $element;
  }





}