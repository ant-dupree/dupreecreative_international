<?php

/**
 * @file
 * Provides views data and hooks for dupree_dedupe module.
 */

use Drupal\views\ViewExecutable;
use Drupal\views\Analyzer;

function dupree_dedupe_views_pre_view(ViewExecutable $view, $display_id, array &$args) {

  // Sometimes there is a view without a name when this function runs ??
  if (isset($view->element['#name'])) {
    // Portfoliio(case studies) by service type on case study page
    // Articles block on articles page
    if ($view->element['#name'] == 'dupree_portfolio_by_service' || $view->element['#name'] == 'dupree_articles_block') {
      // Load the current node and get nid
      $node = \Drupal::routeMatch()->getParameter('node');
      if ($node != null) {
        $nodeId = $node->id();
      }
      else {
        $nodeId = 0;
      }
      $args[] = $nodeId;
    }
  }
}