<?php

namespace Drupal\slm_options\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class OptionsForm.
 *
 * @package Drupal\slm_options\Form
 */
class OptionsForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  public function __construct(
    ConfigManager $config_manager,
    ConfigFactory $config_factory
  ) {
    $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.manager'),
      $container->get('config.factory')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'options_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('slm_options.listoptions');
    $options = $config->get('slm_options');

    $weight = 1;

    foreach($options as $key => $option) {
      if ($option['parent'] == 0) {
        $form['option'][$key]['title'] = array(
            '#markup' => '<h3>' . $option['name'] . '</h3>',
            '#weight' => $weight,
        );
      }
      else {
        $form['option'][$key] = array(
            '#type' => 'checkbox',
            '#title' => $option['name'] . ' £' . $option['price'],
            '#description' => $option['desc'],
        );
      }

    }




    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
