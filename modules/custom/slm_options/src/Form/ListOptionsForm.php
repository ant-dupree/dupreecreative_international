<?php

namespace Drupal\slm_options\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Url;

/**
 * Class ListOptionsForm.
 *
 * @package Drupal\slm_options\Form
 */
class ListOptionsForm extends ConfigFormBase
{

    /**
     * Drupal\Core\Config\ConfigManager definition.
     *
     * @var Drupal\Core\Config\ConfigManager
     */
    protected $configManager;
    /**
     * Drupal\Core\Config\ConfigFactory definition.
     *
     * @var Drupal\Core\Config\ConfigFactory
     */
    protected $configFactory;

    public function __construct(
        ConfigFactoryInterface $config_factory,
        ConfigManager $config_manager
    )
    {
        parent::__construct($config_factory);
        $this->configManager = $config_manager;
        $this->configFactory = $config_factory;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
            $container->get('config.manager')
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'slm_options.listoptions',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'list_options_form';
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {

        // Gert list of options
        $config = $this->config('slm_options.listoptions');
        $options = $config->get('slm_options');

        // Options must be sorted into correct order so we get  parent - children in the correct order
        //
        $sortedOptions = array();


        foreach ($options as $optionId => $option) {
            // Get root options
            if ($option['parent'] == 0) {
                $sortedOptions[$optionId] = $options[$optionId];
                $sortedOptions[$optionId]['depth'] = 0;
                self::getSiblings($optionId, $options, $sortedOptions);
                //print_r($sortedOptions);die;
            }
        }

        $delta = count($options);


        // Start building form
        $form['table-row'] = array(
            '#type' => 'table',
            '#header' => array(t('Name'), t('Weight'), t('Operations'), t('Depth')),
            '#empty' => t('There are no options yet. <a href="@add-url">Add an item.</a>',
                array('@add-url' => Url::fromRoute('<front>')->toString())),
            // TableDrag: Each array value is a list of callback arguments for
            // drupal_add_tabledrag(). The #id of the table is automatically prepended;
            // if there is none, an HTML ID is auto-generated.
            '#tabledrag' => array(
                array(
                    'action' => 'match',
                    'relationship' => 'parent',
                    'group' => 'row-pid',
                    'source' => 'row-id',
                    'hidden' => TRUE,  /* hides the WEIGHT & PARENT tree columns below */
                    'limit' => FALSE,
                ),
                array(
                    'action' => 'order',
                    'relationship' => 'sibling',
                    'group' => 'row-weight',
                ),
            ),
        );


        foreach ($sortedOptions as $id => $option) {

            // TableDrag: Mark the table row as draggable.
            $form['table-row'][$id]['#attributes']['class'][] = 'draggable';

            $indentation = array();

            //  indent item on load
            if (isset($option['depth']) && $option['depth'] > 0) {
                $indentation = [
                    '#theme' => 'indentation',
                    '#size' => $option['depth'],
                ];
            }

            // Some table columns containing raw markup.
            $form['table-row'][$id]['name'] = array(
                '#markup' => $option['name'],
                '#prefix' => !empty($indentation) ? drupal_render($indentation) : '',
            );

            /*$form['table-row'][$id]['price'] = array(
                '#markup' => $option['price'],
                '#prefix' => '£',
            );*/

            // is hidden from #tabledrag array (above)
            // TableDrag: Weight column element.
            $form['table-row'][$id]['weight'] = array(
                '#type' => 'weight',
                '#title' => t('Weight for ID @id', array('@id' => $id)),
                '#title_display' => 'invisible',
                '#default_value' => $option['weight'],
                // Classify the weight element for #tabledrag.
                '#attributes' => array(
                    'class' => array('row-weight')
                ),
            );

            $operations = [
                'edit' => [
                    'title' => $this->t('Edit'),
                    //'query' => $destination,
                    'url' => Url::fromRoute('slm_options.edit_option_form', array('id' => $id)),
                ],
                'delete' => array(
                    'title' => $this->t('Delete'),
                    //'query' => $destination,
                    'url' => Url::fromRoute('slm_options.delete_form', array('id' => $id)),
                ),
            ];
            $form['table-row'][$id]['operations'] = array(
                '#type' => 'operations',
                '#links' => $operations,
            );

            $form['table-row'][$id]['parent']['id'] = [
                '#parents' => ['table-row', $id, 'id'],
                '#type' => 'number',
                '#size' => 3,
                '#min' => 0,
                '#title' => t('Item ID'),
                '#value' => $id,
                '#attributes' => [
                    'class' => ['row-id'],
                ],
            ];

            //print_r($id);

            $form['table-row'][$id]['parent']['pid'] = [
                '#parents' => ['table-row', $id, 'pid'],
                '#type' => 'number',
                '#size' => 3,
                '#min' => 0,
                '#title' => t('Parent ID'),
                '#default_value' => $option['parent'],
                '#attributes' => [
                    'class' => ['row-pid'],
                ],
            ];

            $form['table-row'][$id]['parent']['depth'] = [
                '#parents' => ['table-row', $id, 'depth'],
                '#type' => 'number',
                '#size' => 3,
                '#min' => 0,
                '#title' => t('Depth'),
                '#default_value' => $option['depth'],
                '#attributes' => [
                    'class' => ['row-depth'],
                ],
            ];


        }


        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $orderedRows = $form_state->getValue('table-row');

        $config = \Drupal::service('config.factory')->getEditable('slm_options.listoptions');
        
        $currentOptions = $config->get('slm_options');

        $orderedOptions = array();
        
        foreach ($orderedRows as $key => $row) {

            $orderedOptions[$key] = $currentOptions[$key];
            $orderedOptions[$key]['parent'] = $row['pid'];
            $orderedOptions[$key]['weight'] = $row['weight'];

            // @todo : do something about weights here

        }

        $config->set('slm_options', $orderedOptions)->save();

    }


    /**
     * @param $optiondId
     * @param $options
     * @param array $sortedOptions
     * @return array
     */
    public function getSiblings($optiondId, $options, &$sortedOptions = array())
    {

        foreach ($options as $key => $option) {
            if ($option['parent'] == $optiondId) {
                $sortedOptions[$key] = $options[$key];
                $sortedOptions[$key]['depth'] = $sortedOptions[$optiondId]['depth'] + 1;
                self::getSiblings($key, $options, $sortedOptions);
            }
        }
        return $sortedOptions;
    }
}
