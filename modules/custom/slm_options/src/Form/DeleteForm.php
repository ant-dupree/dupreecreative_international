<?php

namespace Drupal\slm_options\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Url;

/**
 * Class DeleteForm.
 *
 * @package Drupal\slm_options\Form
 */
class DeleteForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  public function __construct(
    ConfigFactoryInterface $config_factory,
      ConfigManager $config_manager,
    ConfigFactory $config_factory
    ) {
    parent::__construct($config_factory);
        $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
            $container->get('config.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'slm_options.delete',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('slm_options.delete');


    $caption = '';

    $caption .= '<p>' . t('This action cannot be undone.') . '</p>';

    $form['delete'] = [
        '#markup' => $caption,
    ];

    $destination = Url::fromRoute('slm_options.delete_form')->toString();

   /*$form['cancel_link'] = [
        '#title' => $this->t('Cancel'),
        '#type' => 'button',
        '#url' => Url::fromRoute('slm_options.list_options_form')
    ];*/

    $form_id = $this->getFormId();

    $cancel_submit = $cancel_submit = function_exists($form_id . '_cancel') ? $form_id . '_cancel' : array($this, 'standardCancel');

    $form['actions']['cancel'] = array(
        '#type' => 'submit',
        '#value' => 'Cancel',
        '#submit' => array($cancel_submit),
        '#validate' => array(),
        '#limit_validation_errors' => array(),
    );


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = \Drupal::service('config.factory')->getEditable('slm_options.listoptions');

    $currentOptions = $config->get('slm_options');

    $optionId = $form_state->getValue('id');

    unset($currentOptions[$optionId]);

    $config->set('slm_options', $currentOptions)->save();

  }


  /**
   * {@inheritdoc}
   */
  public function standardCancel(array &$form, FormStateInterface $form_state)
  {
    $form_state->setRedirect('slm_options.list_options_form');
  }
}
