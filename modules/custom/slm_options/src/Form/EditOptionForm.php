<?php

namespace Drupal\slm_options\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class EditOptionForm.
 *
 * @package Drupal\slm_options\Form
 */
class EditOptionForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  public function __construct(
    ConfigFactoryInterface $config_factory,
      ConfigManager $config_manager,
    ConfigFactory $config_factory
    ) {
    parent::__construct($config_factory);
        $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
            $container->get('config.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'slm_options.listoptions',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'edit_option_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('slm_options.listoptions');
    $optionList = $config->get('slm_options');
    $editItem = \Drupal::request()->get('id');

    $form['id'] = [
        '#type' => 'hidden',
        '#value' => $editItem,
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Option Name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $optionList[$editItem]['name'],
    ];

    $form['description'] = array(
        '#type' => 'textarea',
        '#default_value' => $optionList[$editItem]['desc'],
        '#description' => t('Enter a description for this option.'),
        '#title' => t('Description'),
    );

    $form['price'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Option Price'),
        '#maxlength' => 6,
        '#size' => 6,
        '#default_value' => $optionList[$editItem]['price'],
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = \Drupal::service('config.factory')->getEditable('slm_options.listoptions');

    $currentOptions = $config->get('slm_options');

    $optionId = $form_state->getValue('id');

    $currentOptions[$optionId]['name'] = $form_state->getValue('name');
    $currentOptions[$optionId]['desc'] = $form_state->getValue('description');
    $currentOptions[$optionId]['price'] = $form_state->getValue('price');

    $config->set('slm_options', $currentOptions)->save();

    $form_state->setRedirect('slm_options.list_options_form');
  }

}
