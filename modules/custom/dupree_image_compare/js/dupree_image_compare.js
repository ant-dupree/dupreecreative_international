(function ($) {
    'use strict';
    Drupal.behaviors.dupreeImageCompare = {
        attach: function (context, settings) {
            $('#block-dupreeimagecompare').imagesLoaded( function() {
                var imageCompareContainers = settings.dupree_image_compare;
                var i;
                for(i = 0; i < imageCompareContainers.length; i++) {
                    var element = '#' + imageCompareContainers[i];
                    console.log(imageCompareContainers[i]);
                    $(element).twentytwenty();
                }
            });
        }
    }
})(jQuery);