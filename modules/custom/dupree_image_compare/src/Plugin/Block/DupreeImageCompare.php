<?php

namespace Drupal\dupree_image_compare\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\file\Entity\File;

/**
 * Provides a 'DupreeImageCompare' block.
 *
 * @Block(
 *  id = "dupree_image_compare",
 *  admin_label = @Translation("Dupree image compare"),
 * )
 */
class DupreeImageCompare extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;
  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityManager $entity_manager, 
	QueryFactory $entity_query
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
    $this->entityQuery = $entity_query;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('entity.query')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $imageCompare = [];

    $nids = $this->getNodes();

    if (empty($nids)) {
      // Nothing to do here
      $build['content']['#markup'] = 'No published content.';
      return $build;
    }

    $node_storage = $this->entityManager->getStorage('node');
    $nodes = $node_storage->loadMultiple($nids);

    $count = 0;

    $imageContainers = [];

    foreach ($nodes as $node) {

      $originalImageId = $node->field_original_image->getvalue();
      $originalImageId = $originalImageId[0]['target_id'];
      $modifiedImageId = $node->field_modified_image->getvalue();
      $modifiedImageId = $modifiedImageId[0]['target_id'];

      $originalImageUrl = $this->getImageUrl($originalImageId, 'image_compare');
      $originalImageRenderArray = $this->getRenderImage($originalImageId, 'image_compare');

      $modifiedImageUrl = $this->getImageUrl($modifiedImageId, 'image_compare');
      $modifiedImageRenderArray = $this->getRenderImage($modifiedImageId, 'image_compare');

      $imageInformation = $node->body->value;

      $imageCompare[] = [
        'image_container' => 'ic-container-' . $count,
        'image_title' => $node->title->value,
        'image_original' => $originalImageRenderArray,
        'image_modified' => $modifiedImageRenderArray,
        'image_story' => $imageInformation
      ];

      $imageContainers[] = 'ic-container-' . $count;

      $count++;
    }




    $build = [
      '#theme' => 'dupree_image_compare',
      '#content' => $imageCompare,
      '#attached' => [
        'library' => [
          'dupree_image_compare/dupree_image_compare_library',
        ],
        'drupalSettings' => [
          'dupree_image_compare' => $imageContainers
        ],
      ],
    ];

    //$build['#attached']['drupalSettings']['our_location']['our_location_mapping'] = $imageContainers;

    return $build;

  }

  /**
   * @return array|int
   */
  public function getNodes() {
    // Query DB to get Image Compare Nids
    $query = $this->entityQuery->get('node')
      ->condition('type', 'image_compare')
      ->condition('status', 1);
    $nids = $query->execute();
    return $nids;
  }

  /**
   * @param $pageImageValue
   * @param $pageId
   * @return array
   */
  protected function getRenderImage($workImage, $imageStyle) {

    $file = File::load($workImage);
    // The image.factory service will check if our image is valid.
    $image = \Drupal::service('image.factory')->get($file->getFileUri());

    if ($image->isValid()) {

      // Generate the render array for image
      $imageRenderArray = [
        '#theme' => 'image_style',
        '#style_name' => $imageStyle,
        '#uri' => $file->getFileUri(),
        '#width' => $image->getWidth(),
        '#height' => $image->getHeight(),
        '#attributes' => array('class' => array($imageStyle)),
      ];

      // Add the file entity to the cache dependencies.
      // This will clear our cache when this entity updates.
      $renderer = \Drupal::service('renderer');
      $renderer->addCacheableDependency($imageRenderArray, $file);

      return $imageRenderArray;
    }
  }

  /**
   * @param null $imageValue
   * @param $image_style
   * @return \Drupal\Core\GeneratedUrl|string
   */
  protected function getImageUrl($imageValue = NULL, $image_style) {
    $file = File::load($imageValue);
    $path = $file->getFileUri();
    $imageUrl = \Drupal\image\Entity\ImageStyle::load($image_style)
      ->buildUrl($path);

    return $imageUrl;
  }

}

