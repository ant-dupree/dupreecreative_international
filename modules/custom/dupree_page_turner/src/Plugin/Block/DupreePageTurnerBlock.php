<?php

namespace Drupal\dupree_page_turner\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\file\Entity\File;
use Drupal\Core\Image\ImageInterface;

/**
 * Provides a 'DupreePageTurnerBlock' block.
 *
 * @Block(
 *  id = "dupree_page_turner_block",
 *  admin_label = @Translation("Dupree page turner block"),
 * )
 */
class DupreePageTurnerBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;
  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityManager $entity_manager, QueryFactory $entity_query) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
    $this->entityQuery = $entity_query;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('entity.query')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {

    $nids = $this->getPages();

    if (count($nids) > 0) {
      $node_storage = $this->entityManager->getStorage('node');
      $nodes = $node_storage->loadMultiple($nids);
    }

    // Pages will contain two pages except the first and last
    // which will contain 1
    $bookPages = [];

    // Determine how many pages
    $pageCount = count($nids);

    if ($pageCount % 2 !== 0 || $pageCount < 4) {
      // Bail out if the page count is not even or less than 4
      return [
        '#markup' => $this->t('There must be an even number of pages. You have @pages pages', array('@pages' => $pageCount))
      ];
    }

      foreach ($nodes as $node) {
      $pageField = $node->get('field_page');
      $pageFieldValue = $pageField->value;
      $pageImage = $node->get('field_page_image');
      $pageImageValue = $pageImage->getValue();
      $pageImageValue = $pageImageValue[0];
      $imageUrl = $this->getImageUrl($pageImageValue);

      $bookPages[] = [
        'page' => $pageFieldValue,
        'image' => $imageUrl,
      ];
    }

    return [
      '#theme' => 'block__dupree_book',
      '#content' => $bookPages,
      '#attached' => [
      'library' => [
        'dupree_page_turner/dupree_page_turner_library',
      ],
    ],

    ];
  }




  /**
   * @return array|int
   */
  public function getPages() {
    $query = $this->entityQuery->get('node')
      ->condition('type', 'page_turner_page')
      ->condition('status', 1)
      ->sort('field_page', 'ASC');

    $nids = $query->execute();

    return $nids;

  }

  protected function getImageUrl($pageImageValue = NULL) {
    $file = File::load($pageImageValue['target_id']);
    $path = $file->getFileUri();
    $imageUrl = \Drupal\image\Entity\ImageStyle::load('book_image')->buildUrl($path);

    return $imageUrl;
  }

  /**
   * @param $page
   * @return array
   */
  protected function doFirstRow($page) {
    $pageField = $page->get('field_page');
    $pageFieldValue = $pageField->value;
    $pageImage = $page->get('field_page_image');
    $pageImageValue = $pageImage->getValue();
    $pageImageValue = $pageImageValue[0];

    $imageRenderArray = self::getRenderImage($pageImageValue, $pageFieldValue);

    return [
      'page_left' => '',
      'page_right' => $pageFieldValue,
      'image_left' => '',
      'image_right' => $imageRenderArray,
      'page_numbers' => $pageFieldValue,
    ];
  }

  /**
   * @param $pageImageValue
   * @param $pageId
   * @return array
   */
  protected function getRenderImage($pageImageValue, $pageId) {
    $file = File::load($pageImageValue['target_id']);


    // The image.factory service will check if our image is valid.
    $image = \Drupal::service('image.factory')->get($file->getFileUri());

    if ($image->isValid()) {


      // Generate the render array for image
      $imageRenderArray = [
        '#theme' => 'image_style',
        '#style_name' => 'book_image',
        '#uri' => $file->getFileUri(),
        '#width' => $image->getWidth(),
        '#height' => $image->getHeight(),
        '#attributes' => array('class' => array('page-' . $pageId)),
      ];

      // Add the file entity to the cache dependencies.
      // This will clear our cache when this entity updates.
      $renderer = \Drupal::service('renderer');
      $renderer->addCacheableDependency($imageRenderArray, $file);

      return $imageRenderArray;
    }
  }

  /**
   * @param $page
   * @return array
   */
  protected function doLastRow($page) {
    $pageField = $page->get('field_page');
    $pageFieldValue = $pageField->value;
    return [
      'pageLeft' => '',
      'pageRight' => $pageFieldValue,
      'image_left' => '',
      'image_right' => '',
      'page_numbers' => $pageFieldValue,
    ];
  }

  /**
   * @param null $pageLeft
   * @param null $pageRight
   * @return array
   */
  protected function doDoublePages($pageLeft = null, $pageRight = null) {
    // Left Pages
    $pageLeftField = $pageLeft->get('field_page');
    $pageLeftFieldValue = $pageLeftField->value;
    $pageImage = $pageLeft->get('field_page_image');
    $pageImageValue = $pageImage->getValue();
    $pageImageValue = $pageImageValue[0];
    $imageLeftRenderArray = self::getRenderImage($pageImageValue, $pageLeftFieldValue);

    // Right Pages
    $pageRightField = $pageRight->get('field_page');
    $pageRightFieldValue = $pageRightField->value;
    $pageImage = $pageRight->get('field_page_image');
    $pageImageValue = $pageImage->getValue();
    $pageImageValue = $pageImageValue[0];
    $imageRightRenderArray = self::getRenderImage($pageImageValue, $pageRightFieldValue);

    return [
      'pageLeft' => $pageLeftFieldValue,
      'pageRight' => $pageRightFieldValue,
      'image_left' => $imageLeftRenderArray,
      'image_right' => $imageRightRenderArray,
      'page_numbers' => $pageLeftFieldValue . '-' . $pageRightFieldValue,
    ];
  }



}
