(function ($, Drupal) {

    'use strict';


    var animateTurn = null;


    function disableControls(page) {

    }


    function loadApp() {

        var flipbook = $('.flipbook');

        // Create the flipbook

        flipbook.turn({

            // Width

            width: 1200,

            // Height

            height: 600,

            // Elevation

            elevation: 50,

            // Enable gradients

            gradients: true,

            // Auto center this flipbook

            autoCenter: true,

            page: 2,

            inclination: 50,

            acceleration: true

        });

        $('.flipbook').bind('turning', function (event, page, view) {
            // if less than 480 grater than 767
            var viewPortHeight = $(window).height();
            if (viewPortHeight < 480 || viewPortHeight > 767) {
                if (view[0] == 4) {
                    $('#pt-title-container').fadeOut('slow');
                }
                if (view[0] == 2) {
                    $('#pt-title-container').fadeIn('slow');
                }
            }

        });

        $('.flipbook').bind('first', function (event) {

            $('#pt-title-container').fadeIn('slow');

        });

        // Resize after created
        //resizeViewport();

        // Events for the next button

        $('.next-button').bind($.mouseEvents.over, function () {

            $(this).addClass('next-button-hover');

        }).bind($.mouseEvents.out, function () {

            $(this).removeClass('next-button-hover');

        }).bind($.mouseEvents.down, function () {

            $(this).addClass('next-button-down');

        }).bind($.mouseEvents.up, function () {

            $(this).removeClass('next-button-down');

        }).click(function () {

            $('.flipbook').turn('next');


        });

        // Events for the previous button

        $('.previous-button').bind($.mouseEvents.over, function () {

            $(this).addClass('previous-button-hover');

        }).bind($.mouseEvents.out, function () {

            $(this).removeClass('previous-button-hover');

        }).bind($.mouseEvents.down, function () {

            $(this).addClass('previous-button-down');

        }).bind($.mouseEvents.up, function () {

            $(this).removeClass('previous-button-down');

        }).click(function () {

            $('.flipbook').turn('previous');

        });

        // Bottom page turning buttons


        // Next button
        $('.bottom-next-button').bind($.mouseEvents.over, function () {
            $(this).addClass('bottom-next-button-hover');
        }).click(function () {
            var bookPages = $('.flipbook').turn('pages') - 1;
            if ($('.flipbook').turn('page') < bookPages) {
                $('#pt-title-container').fadeOut('slow');
                $('.flipbook').turn('next');
            }
        });


        // Previous Button
        $('.bottom-previous-button').bind($.mouseEvents.over, function (event, page, view) {
            $(this).addClass('bottom-previous-button-hover');
        }).click(function () {
            // We are starting from page two so don't go back to front cover
            if ($('.flipbook').turn('page') > 3) {
                $('.flipbook').turn('previous');
            }
        });

        // First Page
        $('.bottom-first-button').bind($.mouseEvents.over, function (event, page, view) {
            $(this).addClass('bottom-first-button-hover');
        }).click(function () {
            // We are starting from page two so don't go back to front cover
            if ($('.flipbook').turn('page') > 2) {
                $('#pt-title-container').fadeIn('slow');
                $('.flipbook').turn('page', 2);
            }
        });

        // Last Page
        $('.bottom-last-button').bind($.mouseEvents.over, function (event, page, view) {
            $(this).addClass('bottom-last-button-hover');
        }).click(function () {
            // We are starting from page two so don't go back to front cover
            var bookPages = ($('.flipbook').turn('pages'));
            if ($('.flipbook').turn('page') < bookPages) {
                $('#pt-title-container').fadeOut('slow');
                $('.flipbook').turn('page', bookPages);
            }
        });

        // Play Pages
        $('.bottom-play-button').bind($.mouseEvents.over, function (event, page, view) {
            $(this).addClass('bottom-play-button-hover');
        }).click(function () {

            // Check if not already playing
            if (animateTurn != null) {
                stop();
            }
            else {
                $('#pt-title-container').fadeOut('slow');
                autoTurnPage();
            }
        });

        function autoTurnPage() {
            // Play from current page
            var currentPage = $('.flipbook').turn('page');
            var pageCount = $('.flipbook').turn('pages') - 1;
            if (currentPage < pageCount) {
                $('.flipbook').turn('next');
                animateTurn = setTimeout(autoTurnPage, 2000);
            }
            else {
                stop();
            }
        }

        stop = function () {
            clearTimeout(animateTurn);
            animateTurn = null;
        }


        // Window resize function

        $(window).resize(function () {
            resizeViewport();
        }).bind('orientationchange', function () {
            resizeViewport();
        });


        //** Zoom **/
        $('.flipbook-viewport').zoom({
            flipbook: $('.flipbook'),

            max: function () {

                return largeflipbookWidth() / $('.flipbook').width();

            },

            when: {

                swipeLeft: function () {

                    $(this).zoom('flipbook').turn('next');

                },

                swipeRight: function () {

                    $(this).zoom('flipbook').turn('previous');

                },

                resize: function (event, scale, page, pageElement) {

                    if (scale == 1)
                        loadSmallPage(page, pageElement);
                    else
                        loadLargePage(page, pageElement);

                },

                zoomIn: function () {

                    $('.thumbnails').hide();
                    $('.made').hide();
                    $('.flipbook').removeClass('animated').addClass('zoom-in');
                    $('.zoom-icon').removeClass('zoom-icon-in').addClass('zoom-icon-out');

                    if (!window.escTip && !$.isTouch) {
                        escTip = true;

                        $('<div />', {'class': 'exit-message'}).html('<div>Press ESC to exit</div>').appendTo($('body')).delay(2000).animate({opacity: 0}, 500, function () {
                            $(this).remove();
                        });
                    }
                },

                zoomOut: function () {

                    $('.exit-message').hide();
                    $('.thumbnails').fadeIn();
                    $('.made').fadeIn();
                    $('.zoom-icon').removeClass('zoom-icon-out').addClass('zoom-icon-in');

                    setTimeout(function () {
                        $('.flipbook').addClass('animated').removeClass('zoom-in');
                        resizeViewport();
                    }, 0);

                }
            }
        });


        // Set the width and height for the viewport

        function resizeViewport() {

            var width = $(window).width(),
                height = $(window).height(),
                options = $('.flipbook').turn('options');

            $('.flipbook').removeClass('animated');

            $('.flipbook-viewport').css({
                width: width,
                height: height
            }).zoom('resize');


            if ($('.flipbook').turn('zoom') == 1) {
                var bound = calculateBound({
                    width: options.width,
                    height: options.height,
                    boundWidth: Math.min(options.width, width),
                    boundHeight: Math.min(options.height, height)
                });

                if (bound.width % 2 !== 0)
                    bound.width -= 1;


                if (bound.width != $('.flipbook').width() || bound.height != $('.flipbook').height()) {

                    $('.flipbook').turn('size', bound.width, bound.height);

                    if ($('.flipbook').turn('page') == 1)
                        $('.flipbook').turn('peel', 'br');

                    $('.next-button').css({
                        height: bound.height,
                        backgroundPosition: '-38px ' + (bound.height / 2 - 32 / 2) + 'px'
                    });
                    $('.previous-button').css({
                        height: bound.height,
                        backgroundPosition: '-4px ' + (bound.height / 2 - 32 / 2) + 'px'
                    });
                }

                $('.flipbook').css({top: -bound.height / 2, left: -bound.width / 2});
            }

            var flipbookOffset = $('.flipbook').offset(),
                boundH = height - flipbookOffset.top - $('.flipbook').height(),
                marginTop = (boundH - $('.thumbnails > div').height()) / 2;

            if (marginTop < 0) {
                $('.thumbnails').css({height: 1});
            } else {
                $('.thumbnails').css({height: boundH});
                $('.thumbnails > div').css({marginTop: marginTop});
            }

            if (flipbookOffset.top < $('.made').height())
                $('.made').hide();
            else
                $('.made').show();

            $('.flipbook').addClass('animated');

        }


        // Calculate the width and height of a square within another square

        function calculateBound(d) {

            var bound = {width: d.width, height: d.height};

            if (bound.width > d.boundWidth || bound.height > d.boundHeight) {

                var rel = bound.width / bound.height;

                if (d.boundWidth / rel > d.boundHeight && d.boundHeight * rel <= d.boundWidth) {

                    bound.width = Math.round(d.boundHeight * rel);
                    bound.height = d.boundHeight;

                } else {

                    bound.width = d.boundWidth;
                    bound.height = Math.round(d.boundWidth / rel);

                }
            }

            return bound;
        }
    }

    //$('.flipbook').turn("resize");


    loadApp();
    $(window).trigger('resize');


})(jQuery, Drupal);



