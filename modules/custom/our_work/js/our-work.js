(function ($) {
    Drupal.behaviors.our_work_initial = {
        attach: function (context, settings) {

            // Function to check if element has class
            function hasClass(element, cls) {
                return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
            }

            var viewPortWidth = $(window).width();

            var squareElements = document.getElementsByClassName('our-work-square');
            var height = squareElements[0].clientWidth;
            var elements = document.getElementsByClassName('our-work-background');
            var i = 0;
            for (i = 0; i < elements.length; i++) {
                elements[i].style.height = height + "px";
                //alert('Hello World');
            }
            if (viewPortWidth < 700) {
                var ourWorkElements = document.getElementsByClassName('our-work-landscape');
                var landscapeHeight = Math.floor(height/2);
                for (i = 0; i < ourWorkElements.length; i++) {

                    var childElements = ourWorkElements[i].children[0];
                    console.log('Hello');
                    childElements.firstElementChild.style.height = landscapeHeight + "px";
                }

            }

        }
    };

    Drupal.behaviors.our_work_resize = {
        attach: function (context, settings) {
            $(window).resize(function () {
                var viewPortWidth = $(window).width();

                var squareElements = document.getElementsByClassName('our-work-square');
                var height = squareElements[0].clientWidth;
                var elements = document.getElementsByClassName('our-work-background');
                var i = 0;
                for (i = 0; i < elements.length; i++) {
                    elements[i].style.height = height + "px";
                    //alert('Hello World');
                }

                if (viewPortWidth < 700) {
                    var ourWorkElements = document.getElementsByClassName('our-work-landscape');
                    var landscapeHeight = Math.floor(height/2);
                    for (i = 0; i < ourWorkElements.length; i++) {

                        var childElements = ourWorkElements[i].children[0];
                        console.log('Hello');
                        childElements.firstElementChild.style.height = landscapeHeight + "px";
                    }

                }
            });
        }
    };

})(jQuery);