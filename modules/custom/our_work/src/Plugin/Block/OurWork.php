<?php

namespace Drupal\our_work\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

/**
 * Provides a 'OurWork' block.
 *
 * @Block(
 *  id = "our_work",
 *  admin_label = @Translation("Our work"),
 * )
 */
class OurWork extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;
  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityManager $entity_manager, 
	QueryFactory $entity_query
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
    $this->entityQuery = $entity_query;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('entity.query')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $nids = $this->getnodes();


    // Make it possible to select which node displays
    if (count($nids) > 0) {
      // For now just take first item.
      $nid = reset($nids);
      $node_storage = $this->entityManager->getStorage('node');
      // $nodes = $node_storage->loadMultiple($nids);
      $node = $node_storage->load($nid);
    }
    else {
      $build['#content'] = 'Add some our work content';
      return $build;
    }


    if ($node) {

      $count = 1;
      $numbers = ['one', 'two', 'three', 'four', 'five', 'six'];

      foreach ($numbers as $number) {


        // Get the linked term and image
        $fieldName = 'field_client_' . $number;
        $tid = $node->$fieldName->entity->id();
        $term = $this->entityManager->getStorage('taxonomy_term')->load($tid);
        $logoId = $term->field_hover_image->getvalue();
        $logoId = $logoId[0]['target_id'];

        $logoUrl = $this->getSquareImageUrl($logoId, 'client_logo_120');
        $logoRenderArray = $this->getRenderImage($logoId, 'client_logo_120');


        // Get the referenced node
        $fieldName = 'field_our_work_reference_' . $number;
        $workReference = $node->$fieldName->entity->id();
        $options = array('absolute' => TRUE);
        $workReferenceUrl = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $workReference], $options);
        $workReferenceUrl = $workReferenceUrl->toString();

        // Get the main image
        $fieldName = 'field_our_work_' . $number;
        $workImage = $node->get($fieldName);
        $workImage = $workImage->getValue();
        $workImageId = $workImage[0]['target_id'];
        $imageStyle = '';
        if ($number == 'two' || $number == 'four') {
          $imageStyle = 'latest_work_landscape';
          $imageClass = "our-work-landscape";
        }
        else {
          $imageStyle = 'latest_work_square';
          $imageClass = "our-work-square";
        }
        $imageUrl = $this->getSquareImageUrl($workImageId, $imageStyle);
        $imageRenderArray = $this->getRenderImage($workImageId, $imageStyle);

        $ourWork[] = [
          'link' => $workReferenceUrl,
          'main_image' => $imageUrl,
          'logo_image' => $logoRenderArray,
          'image_class' => $imageClass,
          'row_number' => $number,
        ];

      }

    }

    return [
      '#theme' => 'block__dupree_our_work',
      '#content' => $ourWork,
      '#attached' => [
        'library' => [
          'our_work/our_work_library',
        ],
      ],
    ];
  }

  /**
   * @return array|int
   */
  public function getNodes() {
    $query = $this->entityQuery->get('node')
      ->condition('type', 'our_work')
      ->condition('status', 1);

    $nids = $query->execute();

    return $nids;

  }

  /**
   * @param $pageImageValue
   * @param $pageId
   * @return array
   */
  protected function getRenderImage($workImage, $imageStyle) {

    $file = File::load($workImage);
    // The image.factory service will check if our image is valid.
    $image = \Drupal::service('image.factory')->get($file->getFileUri());

    if ($image->isValid()) {

      // Generate the render array for image
      $imageRenderArray = [
        '#theme' => 'image_style',
        '#style_name' => $imageStyle,
        '#uri' => $file->getFileUri(),
        '#width' => $image->getWidth(),
        '#height' => $image->getHeight(),
        '#attributes' => array('class' => array($imageStyle)),
      ];

      // Add the file entity to the cache dependencies.
      // This will clear our cache when this entity updates.
      $renderer = \Drupal::service('renderer');
      $renderer->addCacheableDependency($imageRenderArray, $file);

      return $imageRenderArray;
    }
  }

  protected function getSquareImageUrl($imageValue = NULL, $image_style) {
    $file = File::load($imageValue);
    $path = $file->getFileUri();
    $imageUrl = \Drupal\image\Entity\ImageStyle::load($image_style)->buildUrl($path);

    return $imageUrl;
  }

}
