<?php
/**
 * Implements hook_views_data_alter().
 */
/**
 * Implements hook_views_data_alter().
 */
function dupree_views_entity_filter_views_data_alter(array &$data) {


  $data['node_field_data']['related_content_titles'] = array(
    'title' => t('Service type filter'),
    'filter' => array(
      'title' => t('Select service type'),
      'help' => t('Specify a list of node titles from an entity reference field.'),
      'field' => 'nid',
      'id' => 'dupree_views_entity_filter_service_types'
    ),
  );

}