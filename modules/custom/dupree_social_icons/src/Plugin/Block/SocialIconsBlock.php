<?php

namespace Drupal\dupree_social_icons\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;

/**
 * Provides a 'SocialIconsBlock' block.
 *
 * @Block(
 *  id = "social_icons_block",
 *  admin_label = @Translation("Social icons block"),
 * )
 */
class SocialIconsBlock extends BlockBase implements ContainerFactoryPluginInterface {


  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var Drupal\Core\Config\ConfigManager
   */
  protected $config_manager;

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config_factory;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        ConfigManager $config_manager, 
	ConfigFactory $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config_manager = $config_manager;
    $this->config_factory = $config_factory;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.manager'),
      $container->get('config.factory')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = \Drupal::config('dupree_social_icons.SocialIcons');
    $content = $config->get('dupree_social_icons');


    $build = [];
    /*$build['#theme'] = 'block__social_icons';
    $build['#content'] = $content;*/
    return array(
      '#theme' => 'block__social_icons',
      '#content' => $content,
      '#attached' => [
        'library' => [
          'dupree_social_icons/social_library',
        ],
      ],
    );

    //return $build;
  }

}
