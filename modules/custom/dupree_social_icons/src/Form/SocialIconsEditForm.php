<?php

namespace Drupal\dupree_social_icons\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class SocialIconsEditForm.
 *
 * @package Drupal\dupree_social_icons\Form
 */
class SocialIconsEditForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  public function __construct(
    ConfigFactoryInterface $config_factory_interface,
    ConfigManager $config_manager,
    ConfigFactory $config_factory
  ) {
    parent::__construct($config_factory_interface);
    $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dupree_social_icons.SocialIcons',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_icons_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $editItem = \Drupal::request()->get('id');
    
    
    $currentConfig = $this->config('dupree_social_icons.SocialIcons');
    $currentOptions = $currentConfig->get('dupree_social_icons');

    $form['service_id'] = [
      '#type' => 'hidden',
      '#value' => $editItem,
    ];

    $form['service_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Name'),
      '#description' => $this->t('Enter the required service name for example &lt;em&gt;Facebook&lt;/em&gt;'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $currentOptions[$editItem]['service_name'],
    ];
    $form['service_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Service URL'),
      '#description' => $this->t('Enter the url of the required service'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $currentOptions[$editItem]['service_url'],
    ];
    $form['fa_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FA Class'),
      '#description' => $this->t('Enter the Font Awesome class'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $currentOptions[$editItem]['fa_class'],
    ];



    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $currentConfig = \Drupal::service('config.factory')->getEditable('dupree_social_icons.SocialIcons');
    $currentOptions = $currentConfig->get('dupree_social_icons');

    $editedItem = $form_state->getValue('service_id');

    $currentOptions[$editedItem]['service_name'] = $form_state->getValue('service_name');
    $currentOptions[$editedItem]['service_url'] = $form_state->getValue('service_url');
    $currentOptions[$editedItem]['fa_class'] = $form_state->getValue('fa_class');

    $currentConfig->set('dupree_social_icons', $currentOptions)->save();

    $form_state->setRedirect('dupree_social_icons.list_social_icons_form');
  }

}
