<?php

namespace Drupal\dupree_social_icons\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class SocialIconsDeleteForm.
 *
 * @package Drupal\dupree_social_icons\Form
 */
class SocialIconsDeleteForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  public function __construct(
    ConfigFactoryInterface $config_factory_interface,
    ConfigManager $config_manager,
    ConfigFactory $config_factory
  ) {
    parent::__construct($config_factory_interface);
    $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dupree_social_icons.SocialIcons',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_icons_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $editItem = \Drupal::request()->get('id');

    $form['service_id'] = [
      '#type' => 'hidden',
      '#value' => $editItem,
    ];

    $form['actions']['cancel']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#submit' => array('::cancelSubmit'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $currentConfig = \Drupal::service('config.factory')->getEditable('dupree_social_icons.SocialIcons');
    $currentOptions = $currentConfig->get('dupree_social_icons');

    unset($currentOptions[$form_state->getValue('service_id')]);

    $currentConfig->set('dupree_social_icons', $currentOptions)->save();

    $form_state->setRedirect('dupree_social_icons.list_social_icons_form');
  }


  /**
   * {@inheritdoc}
   */
  public function cancelSubmit(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $form_state->setRedirect('dupree_social_icons.list_social_icons_form');
  }

}
