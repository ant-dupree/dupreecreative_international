<?php

namespace Drupal\dupree_social_icons\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
//use Drupal\Core\Render\Element\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Url;
/**
 * Class SocialIconAddForm.
 *
 * @package Drupal\dupree_social_icons\Form
 */
class SocialIconAddForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;
  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ConfigManager $config_manager
    ) {
    parent::__construct($config_factory);
    $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dupree_social_icons.SocialIcons',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_icon_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dupree_social_icons.SocialIcons');
    $form['service_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Name'),
      '#description' => $this->t('Enter the required service name for example &lt;em&gt;Facebook&lt;/em&gt;'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('service_name'),
    ];
    $form['service_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Service URL'),
      '#description' => $this->t('Enter the url of the required service'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('service_url'),
    ];
    $form['fa_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FA Class'),
      '#description' => $this->t('Enter the Font Awesome class'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('fa_class'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Get the current configuration
    // $currentConfig = $this->config('dupree_social_icons.SocialIcons');
    // Note loading with previous statement gives an Immutable object so use next
    $currentConfig = \Drupal::service('config.factory')->getEditable('dupree_social_icons.SocialIcons');
    $currentOptions = $currentConfig->get('dupree_social_icons');
    // Need to find the highest key as this is going to be a draggable interface.
    if (count($currentOptions) > 0) {
      $keys = array_keys($currentOptions);
      $newKey = max($keys) + 1;
    }
    else {
      $newKey = 0;
    }

    //$serviceUrl = str_replace('http://', '', $form_state->getValue('service_url'));
    //$urlObject = Url::fromUri($serviceUrl);
    //$link =

    $currentOptions[$newKey]['service_name'] = $form_state->getValue('service_name');
    $currentOptions[$newKey]['service_url'] = $form_state->getValue('service_url');
    $currentOptions[$newKey]['fa_class'] = $form_state->getValue('fa_class');

    $currentConfig->set('dupree_social_icons', $currentOptions)->save();

    $form_state->setRedirect('dupree_social_icons.list_social_icons_form');
  }

}
