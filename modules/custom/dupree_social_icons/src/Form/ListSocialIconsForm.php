<?php

namespace Drupal\dupree_social_icons\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Url;

/**
 * Class ListSocialIconsForm.
 *
 * @package Drupal\dupree_social_icons\Form
 */
class ListSocialIconsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  public function __construct(
    ConfigFactoryInterface $config_factory_interface,
    ConfigManager $config_manager,
    ConfigFactory $config_factory
  ) {
    parent::__construct($config_factory_interface);
    $this->configManager = $config_manager;
    $this->configFactory = $config_factory;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dupree_social_icons.SocialIcons',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'list_social_icons_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $currentConfig = $this->config('dupree_social_icons.SocialIcons');
    $currentOptions = $currentConfig->get('dupree_social_icons');

    //print_($currentOptions);

    $form['table-row'] = array(
      '#type' => 'table',
      '#header' => array(
        $this->t('Service'),
        $this->t('URL'),
        $this->t('Class'),
        $this->t('Operations'),
        $this->t('Weight'),
      ),
      '#empty' => t('No Social Icons available. <a href=":link">Add Social Icon</a>.', array(':link' => \Drupal::url('dupree_social_icons.social_icon_add_form'))),
      // TableDrag: Each array value is a list of callback arguments for
      // drupal_add_tabledrag(). The #id of the table is automatically
      // prepended; if there is none, an HTML ID is auto-generated.
      '#tabledrag' => array(
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ),
      ),
    );


    // The options are always saved in order so there is no need to sort them
    foreach ($currentOptions as $key => $option) {

      $form['table-row'][$key]['#attributes']['class'][] = 'draggable';
      // TableDrag: Sort the table row according to its existing/configured
      // weight.
      $form['table-row'][$key]['#weight'] = $key;

      // Some table columns containing raw markup.
      $form['table-row'][$key]['service_name'] = array(
        '#markup' => $option['service_name'],
      );
      $form['table-row'][$key]['service_url'] = array(
        '#markup' => $option['service_url'],
      );
      $form['table-row'][$key]['fa_class'] = array(
        '#markup' => $option['fa_class'],
      );

      $operations = [
        'edit' => [
          'title' => $this->t('Edit'),
          //'query' => $destination,
          'url' => Url::fromRoute('dupree_social_icons.social_icons_edit_form', array('id' => $key)),
        ],
        'delete' => array(
          'title' => $this->t('Delete'),
          //'query' => $destination,
          'url' => Url::fromRoute('dupree_social_icons.social_icons_delete_form', array('id' => $key)),
        ),
      ];
      $form['table-row'][$key]['operations'] = array(
        '#type' => 'operations',
        '#links' => $operations,
      );

      // TableDrag: Weight column element.
      $form['table-row'][$key]['weight'] = array(
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', array('@title' => $option['service_name'])),
        '#title_display' => 'invisible',
        '#default_value' => $key,
        // Classify the weight element for #tabledrag.
        '#attributes' => array('class' => array('table-sort-weight')),
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $currentConfig = \Drupal::service('config.factory')->getEditable('dupree_social_icons.SocialIcons');
    $currentOptions = $currentConfig->get('dupree_social_icons');

    $orderedOptions = $form_state->getValue('table-row');

    $newOptions = array();

    $i = 0;

    foreach($orderedOptions as $key => $option) {
      $newOptions[$i]['service_name'] =  $currentOptions[$key]['service_name'];
      $newOptions[$i]['service_url'] =  $currentOptions[$key]['service_url'];
      $newOptions[$i]['fa_class'] =  $currentOptions[$key]['fa_class'];
      $i++;
    }

    $currentConfig->set('dupree_social_icons', $newOptions)->save();

    $form_state->setRedirect('dupree_social_icons.list_social_icons_form');
  }

}
