(function ($) {
    'use strict';

    Drupal.behaviors.toggleButtons = {
        attach: function(context, settings) {
            $('.shareText').click(function(){
                $('div.share-buttons').toggleClass('showButts');
            });
        }
    };

    Drupal.behaviors.toggleMainMenuOn = {
        attach: function(context, settings) {
            $('i.fa.fa-bars').click(function(){
                $('.main-menu-hidden').slideToggle('fast');
            });
        }
    };

    Drupal.behaviors.toggleMainMenuOff = {
        attach: function(context, settings) {
            $('i.fa.fa-times-circle-o').click(function(){
                 $('.main-menu-hidden').slideToggle('fast');
            });
        }
    };

}(jQuery));
