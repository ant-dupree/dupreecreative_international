<?php

/**
 * @file
 * Contains contact_block_entity.page.inc.
 *
 * Page callback for Contact Entity entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Contact Entity templates.
 *
 * Default template: contact_block_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_contact_block_entity(array &$variables) {
  // Fetch ContactBlockEntity Entity Object.
  $contact_block_entity = $variables['elements']['#contact_block_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
