(function ($) {

    // @todo figure out what these do
    Drupal.behaviors.contactAddShowSubmit = {
        attach: function (context, settings) {
            $("input").focus(function(){
                $('.button').addClass('show');
            });
            $('#q5').focus(function() {
                $('.button').addClass('show');
            })
        }
    };


    Drupal.behaviors.contactAddRemoveShow = {
        attach: function (context, settings) {
            $('.button').removeClass('show');
        }
    };

    // Add classes to alter height of regions when on mobile landscape
    Drupal.behaviors.contactLandscape = {
        attach: function (context, settings) {
            var viewPortHeight = $(window).height();
            if (viewPortHeight < 500) {
                // Add class to contact form region to override length
                $('.l-content-four').addClass('contact-mobile-landscape');
            }
        }
    };

    
})(jQuery);
