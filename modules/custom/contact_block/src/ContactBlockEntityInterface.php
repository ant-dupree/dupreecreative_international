<?php

namespace Drupal\contact_block;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Contact Entity entities.
 *
 * @ingroup contact_block
 */
interface ContactBlockEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Contact Entity name.
   *
   * @return string
   *   Name of the Contact Entity.
   */
  public function getName();

  /**
   * Sets the Contact Entity name.
   *
   * @param string $name
   *   The Contact Entity name.
   *
   * @return \Drupal\contact_block\ContactBlockEntityInterface
   *   The called Contact Entity entity.
   */
  public function setName($name);

  /**
   * Gets the Contact Entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Contact Entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Contact Entity creation timestamp.
   *
   * @param int $timestamp
   *   The Contact Entity creation timestamp.
   *
   * @return \Drupal\contact_block\ContactBlockEntityInterface
   *   The called Contact Entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Contact Entity published status indicator.
   *
   * Unpublished Contact Entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Contact Entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Contact Entity.
   *
   * @param bool $published
   *   TRUE to set this Contact Entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\contact_block\ContactBlockEntityInterface
   *   The called Contact Entity entity.
   */
  public function setPublished($published);

}
