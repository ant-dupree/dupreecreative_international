<?php

namespace Drupal\contact_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Render\HtmlEscapedText;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\contact_block\Entity\ContactBlockEntity;

/**
 * Class ContactAddForm.
 *
 * @package Drupal\contact_block\Form
 */
class ContactAddForm extends FormBase {

  protected $step = 1;

  protected $formValues = array();

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  protected $entity_manager;

  public function __construct(EntityManager $entity_manager, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager) {
    $this->entity_manager = $entity_manager;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form_state->setCached(FALSE);


    //$form['#tree'] = TRUE;


    $form['component-wrapper'] = [
      '#type' => 'container',
      '#prefix' => '<div id="contact-add-form-wrapper">',
      '#suffix' => '</div>',
      '#attached' => [
        'library' => [
          'contact_block/contact_block_library',
        ],
      ],
    ];

    if ($this->step == 1) {

      $form['component-wrapper']['q1'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Your First Name'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => ['id' => 'q1'],
        '#weight' => 5
      );
    }

    if ($this->step == 2) {

      $form['component-wrapper']['q2'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Your Last Name'),
        //'#description' => $this->t('Enter your name'),
        //'#placeholder' => $this->t('Your Last Name'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => ['id' => 'q2'],
        '#weight' => 10
      );
    }

    if ($this->step == 3) {
      $form['component-wrapper']['q3'] = array(
        '#type' => 'email',
        '#title' => $this->t('Your Email'),
        //'#description' => $this->t('Enter your email'),
        //'#placeholder' => $this->t('Your Email'),
        '#attributes' => ['id' => 'q3'],
        '#weight' => 20
      );
    }

    if ($this->step == 4) {
      $form['component-wrapper']['q4'] = array(
        '#type' => 'tel',
        '#title' => $this->t('Your Telephone Number'),
        //'#description' => $this->t('Enter your telephone number'),
        '#attributes' => ['id' => 'q4'],
        '#weight' => 30
      );
    }

    if ($this->step == 5) {
      $form['component-wrapper']['q5'] = array(
        '#type' => 'textarea',
        '#title' => $this->t('Your Enquiry'),
        '#cols' => 60,
        '#rows' => 5,
        '#resizable' => 'none',
        '#attributes' => ['id' => 'q5'],
        '#weight' => 40
      );
    }

    if ($this->step == 6) {

      $storage = $form_state->get('answers');

      $output = '<div class="form-item-complete-message">' .
        t('@first @last we will contact you ASAP', array('@first' => $storage['q1'], '@last' => $storage['q2']))
       . '</div>';

      $form['component-wrapper'] = [
        '#markup' => $output,
      ];
    }

    if ($this->step < 6) {
      $form['component-wrapper']['actions']['#type'] = 'actions';


      $form['component-wrapper']['actions']['ajaxnextsubmit'] = [
        '#type' => 'submit',
        '#value' => '\e600',
        '#weight' => 50,
        '#submit' => array('::nextStep'),
        '#ajax' => [
          'callback' => '::ajaxContactAddNextSubmit',
          'wrapper' => 'contact-add-form-wrapper',
          'method' => 'replace',
          'effect' => 'fade',
        ],
      ];

      $form['component-wrapper']['counter'] = [
        '#markup' => '<div class="step-counter">' . $this->step . '/5</div>',
        '#weight' => 60
      ];
    }


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entityType = 'contact_block_entity';
    //$entity_def = $this->entity_manager->getDefinition($entityType);

    $newContact = [
      'name' => $form_state->getValue('your_name'),
      'last_name' => $form_state->getValue('last_name'),
      'email' => $form_state->getValue('your_email'),
      'tel' => $form_state->getValue('your_telephone_number'),
      'enquiry' => new HtmlEscapedText($form_state->getValue('enquiry')),
    ];



    $contact = $this->entity_manager->getStorage($entityType)
      ->create($newContact);
    $contact->save();

  }


  public function nextStep(array &$form, FormStateInterface $form_state) {

    // Get storage
    $storage = $form_state->get('answers');

    // Create key to access input
    $key = 'q' . $this->step;
    // Add value to form_state storage
    $storage[$key] = $form_state->getValue($key);
    $form_state->set('answers', $storage);

    // Increment the step
    $this->step++;

    $form_state->setRebuild();


    // Add contact to database
    if ($this->step == 6) {
      $entityType = 'contact_block_entity';
      //$entity_def = $this->entity_manager->getDefinition($entityType);

      $newContact = [
        'name' => $storage['q1'],
        'last_name' => $storage['q2'],
        'email' => $storage['q3'],
        'tel' => $storage['q4'],
        'enquiry' => new HtmlEscapedText($storage['q5']),
      ];

      $contact = $this->entity_manager->getStorage($entityType)
        ->create($newContact);
      $contact->save();


      // Create and send email.
      $module = 'contact_block';
      $key = 'contact_message';

      // Specify 'to' and 'from' addresses.
      $to = 'martin.dupree@dupreecreative.com';
      $from = $this->config('system.site')->get('mail');

      // Information to pass to email
      $form_values = $form_state->getValues();
      $params = $form_values;
      $params += $newContact;
      $params['message'] = t('@first @last, Thankyou for using our Contact Form. A member of the Dupree Creative Team will contact you shortly',
        ['@first' => $form_state->getValue('your_name'), '@last' => $form_state->getValue('last_name')]);

      $language_code = $this->languageManager->getDefaultLanguage()->getId();
      $send_now = TRUE;
      $result = $this->mailManager->mail($module, $key, $to, $language_code, $params, $from, $send_now);
      if ($result['result'] == TRUE) {
        //drupal_set_message(t('Your message has been sent.'));
      }
      else {
        $message = "Email address: " . $newContact['email'] . ' failed';
        \Drupal::logger('my_module')->error($message);
      }
    }

    if ($this->step > 6) {
      return;
    }
  }

  /**
   * Implements ajax submit callback.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   */
  public function ajaxContactAddNextSubmit(array &$form, FormStateInterface $form_state) {

    return $form['component-wrapper'];

  }


}
