<?php

namespace Drupal\contact_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactForm.
 *
 * @package Drupal\contact_block\Form
 */
class ContactForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['your_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your Name'),
      '#description' => $this->t('Enter your name'),
      '#maxlength' => 64,
      '#size' => 64,
    );
    $form['your_email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Your Email'),
      '#description' => $this->t('Enter your email'),
      '#placeholder' => $this->t('Your Email'),
    );
    $form['your_telephone_number'] = array(
      '#type' => 'tel',
      '#title' => $this->t('Your Telephone Number'),
      '#description' => $this->t('Enter your telephone number'),
    );
    $form['enquiry'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Your Enquiry'),
      '#cols' => 60,
      '#rows' => 5,
      '#resizable' => 'none',
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
        drupal_set_message($key . ': ' . $value);
    }

  }

}
