<?php

namespace Drupal\contact_block\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Contact Entity entities.
 *
 * @ingroup contact_block
 */
class ContactBlockEntityDeleteForm extends ContentEntityDeleteForm {


}
