<?php

namespace Drupal\contact_block;

use Drupal\Component\Render\HtmlEscapedText;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;
use Drupal\Component\Utility\Unicode;

/**
 * Defines a class to build a listing of Contact Entity entities.
 *
 * @ingroup contact_block
 */
class ContactBlockEntityListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Contact Entity ID');
    $header['name'] = $this->t('Name');
    $header['last_name'] = $this->t('Last Name');
    $header['email'] = $this->t('Email Address');
    $header['Enquiry'] = $this->t('Enquiry');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\contact_block\Entity\ContactBlockEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.contact_block_entity.edit_form', array(
          'contact_block_entity' => $entity->id(),
        )
      )
    );
    $row['last_name'] = $entity->getLastName();
    $row['email'] = $entity->getEmail();

    // Truncate the enquiry text for display in table
    $enquiry = new HtmlEscapedText($entity->getEnquiry());
    $enquiry = Unicode::truncate($enquiry, 120);
    

    $row['enquiry'] = $enquiry;
    
    return $row + parent::buildRow($entity);
  }

}
