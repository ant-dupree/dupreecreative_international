<?php

namespace Drupal\contact_block\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\contact_block\ContactBlockEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Contact Entity entity.
 *
 * @ingroup contact_block
 *
 * @ContentEntityType(
 *   id = "contact_block_entity",
 *   label = @Translation("Contact Entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contact_block\ContactBlockEntityListBuilder",
 *     "views_data" = "Drupal\contact_block\Entity\ContactBlockEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\contact_block\Form\ContactBlockEntityForm",
 *       "add" = "Drupal\contact_block\Form\ContactBlockEntityForm",
 *       "edit" = "Drupal\contact_block\Form\ContactBlockEntityForm",
 *       "delete" = "Drupal\contact_block\Form\ContactBlockEntityDeleteForm",
 *     },
 *     "access" = "Drupal\contact_block\ContactBlockEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contact_block\ContactBlockEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "contact_block_entity",
 *   admin_permission = "administer contact entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/contact_block_entity/{contact_block_entity}",
 *     "add-form" = "/admin/structure/contact_block_entity/add",
 *     "edit-form" = "/admin/structure/contact_block_entity/{contact_block_entity}/edit",
 *     "delete-form" = "/admin/structure/contact_block_entity/{contact_block_entity}/delete",
 *     "collection" = "/admin/structure/contact_block_entity",
 *   },
 *   field_ui_base_route = "contact_block_entity.settings"
 * )
 */
class ContactBlockEntity extends ContentEntityBase implements ContactBlockEntityInterface
{

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
    {
        parent::preCreate($storage_controller, $values);
        $values += array(
            'user_id' => \Drupal::currentUser()->id(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName()
    {
        return $this->get('last_name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setLastName($lastName)
    {
        $this->set('last_name', $lastName);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->get('email')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        $this->set('last_name', $email);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEnquiry()
    {
        return $this->get('enquiry')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnquiry($enquiry)
    {
        $this->set('enquiry', $enquiry);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner()
    {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId()
    {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid)
    {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account)
    {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished()
    {
        return (bool)$this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published)
    {
        $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields['id'] = BaseFieldDefinition::create('integer')
            ->setLabel(t('ID'))
            ->setDescription(t('The ID of the Contact Entity entity.'))
            ->setReadOnly(TRUE);
        $fields['uuid'] = BaseFieldDefinition::create('uuid')
            ->setLabel(t('UUID'))
            ->setDescription(t('The UUID of the Contact Entity entity.'))
            ->setReadOnly(TRUE);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Authored by'))
            ->setDescription(t('The user ID of author of the Contact Entity entity.'))
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'user')
            ->setSetting('handler', 'default')
            ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', array(
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
            ))
            ->setDisplayOptions('form', array(
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => array(
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ),
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);


        // Name field definition
        $fields['name'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Name'))
            ->setDescription(t('The name of the Contact Entity entity.'))
            ->setSettings(array(
                'max_length' => 255,
                'text_processing' => 0,
            ))
            ->setDefaultValue('')
            ->setDisplayOptions('view', array(
                'label' => 'above',
                'type' => 'string',
                'weight' => -10,
            ))
            ->setDisplayOptions('form', array(
                'type' => 'string_textfield',
                'weight' => -10,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        // Last name field definition
        $fields['last_name'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Last Name'))
            ->setDescription(t('The contact last name.'))
            ->setSettings(array(
                'max_length' => 255,
                'text_processing' => 0,
            ))
            ->setDefaultValue('')
            ->setDisplayOptions('view', array(
                'label' => 'above',
                'type' => 'string',
                'weight' => -9,
            ))
            ->setDisplayOptions('form', array(
                'type' => 'string_textfield',
                'weight' => -9,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        // Email field definition
        $fields['email'] = BaseFieldDefinition::create('email')
            ->setLabel(t("The sender's email"))
            ->setDescription(t('The email of the person that is sending the contact message.'))
            ->setDefaultValue('')
            ->setDisplayOptions('view', array(
                'label' => 'above',
                'type' => 'string',
                'weight' => -8,
            ))
            ->setDisplayOptions('form', array(
                'type' => 'string_textfield',
                'weight' => -8,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        // Tel field definition
        $fields['tel'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Tel'))
            ->setDescription(t('The contact telephone number.'))
            ->setSettings(array(
                'default_value' => '',
                'max_length' => 255,
                'text_processing' => 0,
            ))
            ->setDisplayOptions('view', array(
                'label' => 'above',
                'type' => 'string',
                'weight' => -7,
            ))
            ->setDisplayOptions('form', array(
                'type' => 'string_textfield',
                'weight' => -7,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);


        // Enquiry field definition
        $fields['enquiry'] = BaseFieldDefinition::create('text_long')
            ->setLabel('Enquiry')
            ->setDescription('The clients enquiry.')
            ->setSettings(array(
                'default_value' => '',
                //'text_processing' => 0,
            ))
            ->setDisplayOptions('view', array(
                'label' => 'hidden',
                'type' => 'text_default',
                'weight' => -6,
            ))
            ->setDisplayConfigurable('view', TRUE)
            ->setDisplayOptions('form', array(
                'type' => 'text_textfield',
                'weight' => -6,
            ))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);


        $fields['status'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Publishing status'))
            ->setDescription(t('A boolean indicating whether the Contact Entity is published.'))
            ->setDefaultValue(TRUE);

        $fields['langcode'] = BaseFieldDefinition::create('language')
            ->setLabel(t('Language code'))
            ->setDescription(t('The language code for the Contact Entity entity.'))
            ->setDisplayOptions('form', array(
                'type' => 'language_select',
                'weight' => 10,
            ))
            ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Changed'))
            ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }

}
