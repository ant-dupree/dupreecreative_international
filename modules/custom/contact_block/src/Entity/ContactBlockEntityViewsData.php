<?php

namespace Drupal\contact_block\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Contact Entity entities.
 */
class ContactBlockEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['contact_block_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Contact Entity'),
      'help' => $this->t('The Contact Entity ID.'),
    );

    return $data;
  }

}
