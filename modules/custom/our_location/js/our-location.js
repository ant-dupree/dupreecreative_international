function initMap() {

    var locZoom = parseInt(drupalSettings.our_location.our_location_mapping.zoom);

    var our_location = {
        lat: parseFloat(drupalSettings.our_location.our_location_mapping.lat),
        lng: parseFloat(drupalSettings.our_location.our_location_mapping.long)
    };

    var company = drupalSettings.our_location.our_location_mapping.company;
    var addressOne = drupalSettings.our_location.our_location_mapping.address_one;
    var addressTwo = drupalSettings.our_location.our_location_mapping.address_two;
    var town = drupalSettings.our_location.our_location_mapping.town;
    var county = drupalSettings.our_location.our_location_mapping.county;
    var postCode = drupalSettings.our_location.our_location_mapping.post_code;
    var telephone = drupalSettings.our_location.our_location_mapping.telephone;
    var image_one = drupalSettings.our_location.our_location_mapping.image_one;


    var contentString = '<div id="our-location-content">' +
        '<span class="address-company">' + company + '</span>' +
        '<span class="address-one">' + addressOne + '</span>' +
        '<span class="address-two">' + addressTwo + '</span>' +
        '<span class="address-town">' + town + '</span>' +
        '<span class="address-county">' + county + '</span>' +
        '<span class="address-post">' + postCode + '</span>' +
        '<span class="address-coords">Lat: ' + our_location.lat + ' Long: ' + our_location.lng + '</span>' +
        '<span class="address-tel">Tel: ' + telephone + '</span>' +
        '<span class="address-image_one"><img src="' + image_one + '" alt="Our Location 1" height="160" width="240"> </span>' +
        '</div>';



    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
        center: our_location,
        scrollwheel: false,
        zoom: locZoom
    });

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 400
    });

    // Create a marker and set its position.
    var image = drupalSettings.our_location.our_location_mapping.marker_url;

    var marker = new google.maps.Marker({
        map: map,
        position: our_location,
        animation: google.maps.Animation.DROP,
        icon: image,
        title: drupalSettings.our_location.our_location_mapping.company
    });

    marker.addListener('click', function() {
        // Load the Marker
        infowindow.open(map, marker);
        // Pan map to compensate
    });



    new google.maps.event.trigger( marker, 'click' );


}

