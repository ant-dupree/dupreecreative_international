<?php

namespace Drupal\our_location\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'OurLocationBlock' block.
 *
 * @Block(
 *  id = "our_location_block",
 *  admin_label = @Translation("Our location block"),
 * )
 */
class OurLocationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Google Maps API key.'),
      '#description' => $this->t('Company or location name'),
      '#default_value' => isset($this->configuration['api_key']) ? $this->configuration['api_key'] : '',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    );

    $form['company_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Company Name'),
      '#description' => $this->t('Company or location name'),
      '#default_value' => isset($this->configuration['company_name']) ? $this->configuration['company_name'] : '',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    );

    $form['address_one'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#description' => $this->t('Address line one'),
      '#default_value' => isset($this->configuration['address_one']) ? $this->configuration['address_one'] : '',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    );

    $form['address_two'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#description' => $this->t('Address line two'),
      '#default_value' => isset($this->configuration['address_two']) ? $this->configuration['address_two'] : '',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    );

    $form['town'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Town/City'),
      '#description' => $this->t('Town or city'),
      '#default_value' => isset($this->configuration['town']) ? $this->configuration['town'] : '',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    );

    $form['county'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('County'),
      '#description' => $this->t('County'),
      '#default_value' => isset($this->configuration['county']) ? $this->configuration['county'] : '',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    );

    $form['post_code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Post Code'),
      '#description' => $this->t('Post Code'),
      '#default_value' => isset($this->configuration['post_code']) ? $this->configuration['post_code'] : '',
      '#maxlength' => 12,
      '#size' => 12,
      '#weight' => '0',
    );

    $form['telephone'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Telephone'),
      '#description' => $this->t('telephone'),
      '#default_value' => isset($this->configuration['telephone']) ? $this->configuration['telephone'] : '',
      '#maxlength' => 18,
      '#size' => 18,
      '#weight' => '0',
    );

    $form['long'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Longitude'),
      '#description' => $this->t('Longitude'),
      '#default_value' => isset($this->configuration['long']) ? $this->configuration['long'] : '',
      '#maxlength' => 20,
      '#size' => 20,
      '#weight' => '0',
    );

    $form['lat'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Latitude'),
      '#description' => $this->t('Latitude'),
      '#default_value' => isset($this->configuration['lat']) ? $this->configuration['lat'] : '',
      '#maxlength' => 20,
      '#size' => 20,
      '#weight' => '0',
    );

    $form['zoom'] = [
      '#type' => 'select',
      '#title' => $this->t('Select zoom level'),
      '#options' => [
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
      ],
      '#default_value' => isset($this->configuration['zoom']) ? $this->configuration['zoom'] : '',
      '#weight' => '0',
    ];

    $form['marker_file'] = array(
      '#type' => 'managed_file',
      '#title' => t('Upload a marker image'),
      '#default_value' => isset($this->configuration['marker_file']) ? $this->configuration['marker_file'] : '',
      '#required' => false,
      '#weight' => '0',
      '#upload_location' => 'public://our_location_markers',
    );

    $form['image_one'] = array(
      '#type' => 'managed_file',
      '#title' => t('Upload a display image'),
      '#default_value' => isset($this->configuration['image_one']) ? $this->configuration['image_one'] : '',
      '#required' => false,
      '#weight' => '0',
      '#upload_location' => 'public://our_location_markers',
    );

    $form['directions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show directions'),
      '#default_value' => isset($this->configuration['directions']) ? $this->configuration['directions'] : '',
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {


    $this->configuration['api_key'] = $form_state->getValue('api_key');
    $this->configuration['company_name'] = $form_state->getValue('company_name');
    $this->configuration['address_one'] = $form_state->getValue('address_one');
    $this->configuration['address_two'] = $form_state->getValue('address_two');
    $this->configuration['town'] = $form_state->getValue('town');
    $this->configuration['county'] = $form_state->getValue('county');
    $this->configuration['post_code'] = $form_state->getValue('post_code');
    $this->configuration['telephone'] = $form_state->getValue('telephone');
    $this->configuration['long'] = $form_state->getValue('long');
    $this->configuration['lat'] = $form_state->getValue('lat');
    $this->configuration['zoom'] = $form_state->getValue('zoom');

    $this->configuration['marker_file'] = $form_state->getValue('marker_file');
    $this->configuration['image_one'] = $form_state->getValue('image_one');

    $this->configuration['directions'] = $form_state->getValue('directions');

    // Make the image files permanent
    $this->makeFilePermanent($form_state->getValue('marker_file'), 'marker');
    $this->makeFilePermanent($form_state->getValue('image_one'), 'image_one');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Add API key to globals
    if (!isset($GLOBALS['gmap_api_key'])) {
      $GLOBALS['gmap_api_key'] = $this->configuration['api_key'];
    }

    $build = [];

    // Collect marker image URL
    $markerFile = $this->configuration['marker_file'];
    if (!empty($markerFile)) {
      $markerFileId = $markerFile[0];
      if ($markerFileId > '') {
        $markerUrl = self::getImageUrl($markerFileId, 'our_location_marker');
      }
      else {
        $markerUrl = '';
      }
    }

    // Collect Image one url
    $imageOneFile = $this->configuration['image_one'];
    if (!empty($imageOneFile)) {
      $imageOneFileId = $imageOneFile[0];
      if ($imageOneFileId > '') {
        $imageOneUrl = self::getImageUrl($imageOneFileId, 'our_location_image');
      }
      else {
        $imageOneUrl = '';
      }
    }

    // @TODO add cacheable dependencies
    // Add the file entity to the cache dependencies.
    // This will clear our cache when this entity updates.
    // $renderer = \Drupal::service('renderer');
    // $renderer->addCacheableDependency($imageRenderArray, $file);
    // @TODO code above does this



    $mappingInfo = [
      'api_key' => $this->configuration['api_key'],
      'long' => $this->configuration['long'],
      'lat' => $this->configuration['lat'],
      'zoom' => $this->configuration['zoom'],
      'marker_url' => isset($markerUrl) ? $markerUrl : '',
      'company' => $this->configuration['company_name'],
      'address_one' => $this->configuration['address_one'],
      'address_two' => $this->configuration['address_two'],
      'town' => $this->configuration['town'],
      'county' => $this->configuration['county'],
      'post_code' => $this->configuration['post_code'],
      'telephone' => $this->configuration['telephone'],
      'image_one' => isset($imageOneUrl) ? $imageOneUrl : '',
    ];

    // Don't show directions
    if (isset($this->configuration['directions']) && $this->configuration['directions'] == true) {
      // Build with directions
      $build = [
        '#theme' => 'block__our_location_directions',
        '#content' => '',
        '#attached' => [
          'library' => [
            'our_location/our_location.directions',
            'our_location/our_location.gmap_api'
          ],
        ],
      ];
    }
    else {
      // Build without
      $build = [
        '#theme' => 'block__our_location_mapping',
        '#content' => '',
        '#attached' => [
          'library' => [
            'our_location/our_location.mapping',
            'our_location/our_location.gmap_api'
          ],
        ],
      ];
    }

    // Pass settings to js
    $build['#attached']['drupalSettings']['our_location']['our_location_mapping'] = $mappingInfo;

    return $build;
  }

  /**
   * @param null $pageImageValue
   * @return \Drupal\Core\GeneratedUrl|string
   */
  protected function getImageUrl($markerFileId = NULL, $image_syle = NULL) {

    $file = File::load($markerFileId);
    $path = $file->getFileUri();
    // Load Themed Image
    $imageUrl = \Drupal\image\Entity\ImageStyle::load($image_syle)->buildUrl($path);

    return $imageUrl;
  }

  /**
   * @param $fid
   * Makes a file permanent
   */
  protected function makeFilePermanent($image, $type) {
    /* Load the object of the file by it's fid */
    $fid = $image[0];
    $file = File::load( $fid );

    /* Set the status flag permanent of the file object */
    $file->setPermanent();

    /* Save the file in database */
    $file->save();

    $file_usage = \Drupal::service('file.usage');
    $file_usage->add($file, 'our_location', $type, 'our_location_block');

  }
}
