function resize(el) {
    // reset the width and height to the css defaults
    el.style.width = '';
    el.style.height = '';

    var width = el.clientWidth,
        height = Math.round(width / this.ratio),
        padded = Math.round(document.body.clientHeight * 0.9);

    // if the height is too big for the window, constrain it
    if (height > padded) {
        height = padded;
        width = Math.round(height * this.ratio);
    }

    // set the width and height matching the aspect ratio
    el.style.width = width + 'px';
    el.style.height = height + 'px';

    return {
        width: width,
        height: height
    };
}