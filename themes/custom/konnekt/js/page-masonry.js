(function ($, Drupal) {

    'use strict';


    // Add classes to alter height of regions when on mobile landscape
    Drupal.behaviors.pageMasonry = {
        attach: function (context, settings) {


            // init Masonry
            var $grid = $('.grid').masonry({
                // options...
                itemSelector: '.grid-item',
                // use element for option
                columnWidth: '.grid-sizer',
                gutter: 0,
                percentPosition: true
            });
            // layout Masonry after each image loads
            $grid.imagesLoaded().progress(function () {
                $grid.masonry('layout');
            });
        }
    };

    Drupal.behaviors.initMasonryStyle = {
        attach: function (context, settings) {

            // Only do this on tablet
            var screenWidth = $(window).width();
            if (screenWidth > 767) {
                $('div[id^="grid-item-"]', context).hover(function () {
                    var titleElement = this.firstElementChild.children[1];
                    $(titleElement).fadeToggle(250);
                });
            }
        }
    };

})(jQuery, Drupal);