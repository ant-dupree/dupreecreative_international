(function ($, Drupal) {

    'use strict';



    // Add classes to alter height of regions when on mobile landscape
    Drupal.behaviors.heroLandscape = {
        attach: function (context, settings) {
            var viewPortHeight = $(window).height();

            if (viewPortHeight < 500) {
                // Add class for mobile landscape for hero block on front page
                $('#block-herofrontpage').addClass('mobile-landscape');
                // Add class to contact form region to override length
                $('.l-content-four').addClass('contact-mobile-landscape');
            }
            // resize main menu items
            if (viewPortHeight < 500) {
                var menuItems = $('.menu-container ul li a');
                var i;
                for (i = 0; i < menuItems.length; i++) {
                    menuItems[i].className += " main-menu-landscape";
                }
            }
        }
    };

    // Hide hero element on front page when scrolling down
    Drupal.behaviors.heroScroll = {
        attach: function (context, settings) {
            var lastScrollTop = 0;
            var viewPortHeight = $(window).height();
            $(window).scroll(function (event) {
                var st = $(this).scrollTop();

                if (viewPortHeight > 400) {
                    if (st > lastScrollTop) {
                        // downscroll code
                        var scrollTop = $(this).scrollTop();
                        if (scrollTop > 60) {
                            $('.block-herofrontpage').fadeOut('slow');
                        }
                    } else {
                        // upscroll code
                        var scrollTop = $(this).scrollTop();
                        if (scrollTop > 300) {
                            $('.block-herofrontpage').fadeIn('slow');
                        }
                    }
                }

                if (viewPortHeight < 400) {
                    if (st > lastScrollTop) {
                        // downscroll code
                        var scrollTop = $(this).scrollTop();
                        if (scrollTop > 20) {
                            $('.block-herofrontpage').fadeOut('slow');
                        }
                    } else {
                        // upscroll code
                        var scrollTop = $(this).scrollTop();
                        if (scrollTop > 300) {
                            $('.block-herofrontpage').fadeIn('slow');
                        }
                    }
                }

                lastScrollTop = st;
            });
        }
    }

    /* Page Turner Landcape */
    Drupal.behaviors.pageTurnerMobileLandscapeAdj = {
        attach: function (context, settings) {
            var viewPortHeight = $(window).height();
            if (viewPortHeight < 480 && viewPortHeight > 380) {
                $(".controllers").css('top', '38%');
            }
            if (viewPortHeight < 380) {
                $(".controllers").css('top', '39%');
            }
        }
    };

    Drupal.behaviors.pageTurnerMobileHeightAdjust = {
        attach: function (context, settings) {
            var viewPortWidth = $(window).width();
            if (viewPortWidth < 736 && viewPortWidth > 400) {
                $("#pt-container").css('top', '44%');
            }
        }
    };


    // Fade out title on Page Turner
    Drupal.behaviors.pageTurnerRegionHeader = {
        attach: function (context, settings) {
            var previousScrollPoint = 0;
            var viewPheight = $(window).height();
            if (viewPheight < 480) {
                $(window).scroll(function (event) {

                    // Hide header on mobile landscape
                    var regionTop = $("#page-turner-region").offset().top - 50; //Top of page turner less 50px
                    var regionHeight = $('#page-turner-region').height(); // Height of page turner
                    var regionBottom = regionTop + regionHeight + 50; // Bottom of region + 50
                    var currPos = $(this).scrollTop();
                    var viewPortHeight = $(window).height();


                    if (currPos > previousScrollPoint) {
                        // Scrolling Down
                        if (currPos > regionTop && currPos < regionBottom) {
                            $('#region-header').fadeOut('slow');
                        }
                        if (currPos > regionBottom) {
                            $('#region-header').fadeIn('slow');
                        }

                    }
                    if (currPos < previousScrollPoint) {
                        // Scrolling Up
                        if (currPos < regionBottom && currPos > regionTop) {
                            $('#region-header').fadeOut('slow');
                        }
                        if (currPos < regionTop) {
                            $('#region-header').fadeIn('slow');
                        }
                    }

                    previousScrollPoint = currPos;
                });
            }
        }
    };


    $(window).resize(function () {
        var viewPortHeight = $(window).height();
        var viewPortWidth = $(window).width();

        if (viewPortHeight > 500) {
            $('#block-herofrontpage').removeClass('mobile-landscape');
        }
        if (viewPortHeight < 500) {
            $('#block-herofrontpage').addClass('mobile-landscape');
        }

        // resize main menu items
        if (viewPortHeight < 500) {
            var menuItems = $('.menu-container ul li a');
            var i;
            for (i = 0; i < menuItems.length; i++) {
                menuItems[i].className += " main-menu-landscape";
            }
        }

        if (viewPortHeight > 500) {
            var menuItems = $('.menu-container ul li a');
            var i;
            var subString = 'main-menu-landscape';
            for (i = 0; i < menuItems.length; i++) {
                var classes = menuItems[i].className;
                if (~classes.indexOf(subString)) {
                    // Leave is-active untouched
                    if (~classes.indexOf('is-active')) {
                        menuItems[i].className = 'is-active';
                    }
                    else {
                        menuItems[i].className = '';
                    }
                }

            }

        }


    });


    Drupal.behaviors.clientSetContainerHeight = {
        attach: function (context, settings) {

            // Get element height with margin
            var clients = document.getElementsByClassName('client');
            var clientHeight = $(clients[0]).outerHeight(true);

            var rows = 4;
            // Calculate the container height
            var containerHeight = clientHeight * rows;
            $('#client-list-container').height(containerHeight + 'px');
        }
    };

    Drupal.behaviors.clientSetExpandedContainerHeight = {
        attach: function (context, settings) {

            function setExpandedHeight() {
                // Get element height with margin
                var clients = document.getElementsByClassName('client');
                var clientHeight = $(clients[0]).outerHeight(true);

                // calculate how many cols in the container
                var viewPortWidth = $(window).width();
                var cols;

                switch (true) {
                    case (viewPortWidth < 464) :
                        cols = 3;
                        break;
                    case (viewPortWidth < 768):
                        cols = 4;
                        break;
                    case (viewPortWidth < 1120):
                        cols = 5;
                        break;
                    case (viewPortWidth > 1119):
                        cols = 6;
                }

                // Calculate how many rows
                var incompleteRow = clients.length % cols;
                var totalRows = Math.floor(clients.length / cols);
                if (incompleteRow > 0) {
                    totalRows++;
                }
                var containerHeight = clientHeight * totalRows;
                $("#client-list-container").animate({height: containerHeight + 'px'}, 'slow', function(){
                    //alert('Transition Complete');
                });
            }

            function setContractedHeight() {
                // Get element height with margin
                var clients = document.getElementsByClassName('client');
                var clientHeight = $(clients[0]).outerHeight(true);

                var rows = 4;
                // Calculate the container height
                var containerHeight = clientHeight * rows;
                $("#client-list-container").animate({height: containerHeight + 'px'}, 'slow', function(){
                    //alert('Transition Complete');
                });
            }

            $('#expand-clients').click(function() {
                setExpandedHeight();
                $('#expand-clients').fadeOut('slow');
                $('#contract-clients').delay(800).fadeIn('slow');
            });

            $('#contract-clients').click(function() {
                setContractedHeight();
                $('#contract-clients').fadeOut('slow');
                $('#expand-clients').delay(800).fadeIn('slow');
            });


        }
    };


})(jQuery, Drupal);
