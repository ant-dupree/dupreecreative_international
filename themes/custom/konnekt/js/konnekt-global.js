(function ($, Drupal) {

    'use strict';

    /**
     * Initialize tableSelects.
     *
     * @type {Drupal~behavior}
     *
     * @prop {Drupal~behaviorAttach} attach
     *   Attaches tableSelect functionality.
     */

    // Resize header region on Scroll
    Drupal.behaviors.headerScroll = {
        attach: function (context, settings) {
            $(window).scroll(function () {
                if ($(window).scrollTop() > 60) {
                    $('.l-header').once().addClass('scrolled-head');
                    $('.social').once().addClass('a-scrolled-position');
                }
            });
        }
    }

    Drupal.behaviors.fitVids = {
        attach: function (context, settings) {
            $(".field--name-body").fitVids();
        }
    };
     // Change video height and width on window load and resize
    $(window).on('load resize', function () {
        resizeSlideContainer();
        //resizeAboutUs();
    });
function resizeSlideContainer() {
        var $slideContainer = $(".views_slideshow_cycle_slide");
        var $slideTeaser = $(".views_slideshow_cycle_teaser_section");
        var $field_content_video_mp4 = $(".views-field-field-video-mp4 .field-content video");

        var slideHeight = $(window).height();

        var slideContainerHeight = $slideContainer.height();

        if (slideContainerHeight != slideHeight) {

          //$slideContainer.height(slideHeight);
            // $field_content_video_mp4.height('100%');
            // $field_content_video_mp4.width('100%');



          //$slideTeaser.height(slideHeight);

         // $field_content.height(slideHeight);

        }
    }
})(jQuery, Drupal);
