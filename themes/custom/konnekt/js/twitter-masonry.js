(function ($, Drupal) {

    'use strict';

    Drupal.behaviors.masonaryTwitter = {
        attach: function (context, settings) {

            function hasClass(element, cls) {
                return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
            }

            var viewPortWidth = $(window).width();

            if (viewPortWidth > 620) {

                // Add rendered event to check if item is rendered.
                twttr.ready(function (twttr) {
                    twttr.events.bind(
                        'rendered',
                        function (event) {
                            //console.log("Rendered widget", event.target.id);
                            // Add class to show item is rendered.
                            event.target.className += " rendered";
                        }
                    );

                });

                // Poll twitter widgets and check if they have rendered by checking for 'rendered' class
                var interval = setInterval(function () {
                    var tWidgets = $('*[id^="twitter-widget-"]');
                    // console.log(tWidgets.length);
                    if (tWidgets.length > 0) {
                        var i;
                        var classCount = 0;
                        for (i = 0; i < tWidgets.length; i++) {
                            if (hasClass(tWidgets[i], 'rendered')) {
                                classCount++;
                                //console.log(tWidgets[i].id, ' has class');
                            }
                        }
                        // if classCount matches tWidgets length then all items have rendered
                        if (classCount == tWidgets.length - 1) {
                            clearInterval(interval);

                            var $grid = $('.grid').masonry({
                                itemSelector: '.grid-item',
                                columnWidth: '.grid-sizer',
                                gutter: 10,
                                // disable initial layout
                                initLayout: false
                            });
                            // add event listener for initial layout
                            $grid.on('layoutComplete', function (event, items) {
                                $grid.masonry('reloadItems');
                            });
                            // trigger initial layout
                            $grid.masonry();
                        }
                    }


                }, 100);
            }
        }
    }

})(jQuery, Drupal);
